## Welcome to m3::m3meka.

m3 is open-source control software provided by Meka Robotics,LLC for it's robots.
This is an update to match with recent configurations and needs at Ensta ParisTech.

> Maintainer : Antoine Hoarau (hoarau.robotics@gmail.com)

[![Build Status](https://travis-ci.org/ahoarau/m3meka.svg?branch=master)](https://travis-ci.org/ahoarau/m3meka)
